.MODEL TINY
.DATA

source_file_handle     DW ?
executable_file_handle DW ?
fbuff                  DB ?

; 00001b - inside a string
; 00010b - inside a comment
; 00100b - reading a number
; 01000b - reading assembly code
; 10000b - expecting a character to put on stack
flags                  DB 00000b

OPCODE_JMP             =  0E9h ; jmp near-label
OPCODE_END             DB 0B4h, 04Ch, 0CDh, 021h ; call dos interrupt to finish program
OPCODE_MOV_DI          =  0BFh ; mov di
OPCODE_MOV_AX          =  0B8h ; mov ax
OPCODE_MOV_AL          =  0B0h ; mov al
OPCODE_ADD_AX_BP       DB 003h, 0C5h ; add ax, bp
OPCODE_CALL_DI         DB 0FFh, 0D7h ; call di
OPCODE_RET             =  0C3h ; ret
OPCODE_INIT_SE         DB 0BEh, 0AAh, 0AAh ; mov se, 0AAAAh

simple_commands_table DB ':'
DW OFFSET runtime_assign
DB ';'
DW OFFSET runtime_get_value
DB '.'
DW OFFSET runtime_print_top_stack
DB '='
DW OFFSET runtime_eq
DB '?'
DW OFFSET runtime_if
DB '#'
DW OFFSET runtime_while
DB '+'
DW OFFSET runtime_add
DB '-'
DW OFFSET runtime_sub
DB '*'
DW OFFSET runtime_mul
DB '/'
DW OFFSET runtime_div
DB '~'
DW OFFSET runtime_bit_not
DB '>'
DW OFFSET runtime_gt
DB '&'
DW OFFSET runtime_bit_and
DB '|'
DW OFFSET runtime_bit_or
DB '$'
DW OFFSET runtime_dup
DB '%'
DW OFFSET runtime_pop
DB '\'
DW OFFSET runtime_swap
DB '@'
DW OFFSET runtime_rot
DB 'O'
DW OFFSET runtime_pick
DB ','
DW OFFSET runtime_write_char
DB '^'
DW OFFSET runtime_read_char
DB '_'
DW OFFSET runtime_negate
DB '!'
DW OFFSET runtime_call_function

end_of_program      DB  ?

.CODE
.STARTUP
begin_runtime:
	
	; Allocating memory on the stack
	; bp +  0 .. bp + 51 - variables
	; bp -  6 .. bp -  1 - number output buffer
	sub sp, 58 ; = 2 * 26 + 6
	mov bp, sp
	add bp, 7 ; 1 + 6

	jmp end_runtime

runtime_if PROC NEAR
	mov di, bx
	call runtime_pop
	mov ax, bx
	call runtime_pop
	cmp ax, 0
	jz if_ret
	call di
	if_ret:
	ret
runtime_if ENDP
	
runtime_while PROC NEAR

	mov cx, bx
	call runtime_pop
	mov dx, bx
	call runtime_pop
	
	_loop:
	push cx
	push dx
	call dx
	pop dx
	pop cx
	mov ax, bx
	call runtime_pop
	cmp ax, 0
	jnz call_body
	ret

	call_body:
	push cx
	push dx
	call cx
	pop dx
	pop cx
	jmp _loop

runtime_while ENDP
	
runtime_add PROC NEAR
	mov ax, bx
	add ax, [si]
	jmp push_value
runtime_add ENDP

runtime_sub PROC NEAR
	mov ax, [si]
	sub ax, bx
	jmp push_value
runtime_sub ENDP

runtime_mul PROC NEAR
	mov ax, bx
	mul WORD PTR [si]
	jmp push_value
runtime_mul ENDP

runtime_div PROC NEAR
	mov ax, [si]
	mov dx, [si]
	mov cl, 15
	sar dx, cl
	idiv bx
	jmp push_value
runtime_div ENDP

runtime_bit_and PROC NEAR
	mov ax, bx
	and ax, [si]
	jmp push_value
runtime_bit_and ENDP

runtime_bit_or PROC NEAR
	mov ax, bx
	or ax, [si]
	jmp push_value
runtime_bit_or ENDP

runtime_bit_not PROC NEAR
	not bx
	ret
runtime_bit_not ENDP

runtime_gt PROC NEAR
	mov ax, 0
	cmp [si], bx
	jle push_value
	not ax
	jmp push_value
runtime_gt ENDP

runtime_eq PROC NEAR

	mov ax, 0
	cmp bx, [si]
	jnz push_value
	not ax
	
	push_value:
	call runtime_pop
	call runtime_pop
	call runtime_push
	ret

runtime_eq ENDP
	
runtime_assign PROC NEAR
	mov di, bx
	call runtime_pop
	mov [di], bx
	call runtime_pop
	ret
runtime_assign ENDP

runtime_write_char PROC NEAR
	mov al, bl
	call runtime_print_char
	call runtime_pop
	ret
runtime_write_char ENDP

runtime_read_char PROC NEAR
	mov ah, 01h
	int 21h
	mov ah, 00h
	call runtime_push
	ret
runtime_read_char ENDP

runtime_pick PROC NEAR
	mov cx, bx
	mov di, si
	sub di, cx
	sub di, cx
	mov bx, [di]
	ret
runtime_pick ENDP

runtime_rot PROC NEAR
	mov dx, bx
	call runtime_pop
	mov ax, bx
	call runtime_pop
	mov cx, bx
	call runtime_pop
	call runtime_push
	mov ax, dx
	call runtime_push
	mov ax, cx
	call runtime_push
	ret
runtime_rot ENDP

runtime_swap PROC NEAR
	xchg bx, [si]
	ret
runtime_swap ENDP

runtime_get_value PROC NEAR
	mov bx, [bx]
	ret
runtime_get_value ENDP
	
runtime_push PROC NEAR
	add si, 2
	mov [si], bx
	mov bx, ax
	ret
runtime_push ENDP

runtime_pop PROC NEAR
	mov bx, [si]
	sub si, 2
	ret
runtime_pop ENDP

runtime_dup PROC NEAR
	mov ax, bx
	call runtime_push
	ret
runtime_dup ENDP

runtime_print_top_stack PROC NEAR
	
	mov di, bp
	dec di
	mov BYTE PTR [di], '$'
	
	mov ax, bx
	cmp ax, 0
	jge itoa
	mov al, '-'
	call runtime_print_char
	mov ax, bx
	neg ax
	
	itoa:
	xor dx, dx
	mov cx, 10
	div cx
	add dl, '0'
	dec di
	mov [di], dl
	cmp ax, 0
	jnz itoa
	
	mov ah, 09h
	mov dx, di
	int 21h
	
	call runtime_pop
	ret
	
runtime_print_top_stack ENDP

runtime_negate PROC NEAR
	neg bx
	ret
runtime_negate ENDP

runtime_print_char PROC NEAR
	mov dl, al
	mov ah, 02h
	int 21h
	ret
runtime_print_char ENDP

runtime_call_function PROC NEAR
	mov ax, bx
	call runtime_pop
	call ax
	ret
runtime_call_function ENDP
	
end_runtime:
	jmp begin
	
proc_symbol PROC NEAR

	test flags, 10000b
	jz _start_comm
	xor ah, ah
	mov al, fbuff
	mov si, ax
	call write_push_value
	xor flags, 10000b
	ret

	_start_comm:
	cmp fbuff, '{'
	jnz _end_comm
	or flags, 00010b
	ret
	
	_end_comm:
	cmp fbuff, '}'
	jnz _comm
	xor flags, 00010b
	ret
	
	_comm:
	test flags, 00010b
	jz _string
	ret
	
	_string: ; A string begins/ends
	cmp fbuff, '"'
	jnz _string_char
	test flags, 00001b
	jnz _end_string
	or flags, 00001b
	ret
	
	_end_string:
	xor flags, 00001b
	ret
	
	_string_char: ; Printing string character
	test flags, 00001b
	jz _cmp_num
	
	mov BYTE PTR [di], OPCODE_MOV_AL
	inc di
	lea si, fbuff
	movsb
	
	mov ax, OFFSET runtime_print_char
	call write_exec
	ret
	
	_cmp_num:
	xor bx, bx
	mov bl, fbuff
	sub bl, '0'
	js _not_num
	cmp fbuff, ':' ; follows '9'
	jnb _not_num
	
	_num:
	test flags, 00100b
	jnz _num_2
	or flags, 00100b
	xor si, si
	_num_2:
	mov ax, si
	mov dx, 10
	mul dx
	add ax, bx
	mov si, ax
	ret
	    
	_not_num:
	test flags, 00100b
	jz _1
	
	test flags, 01000b
	jnz _write_asm
	call write_push_value
	jmp _1
	
	_write_asm:
	mov ax, si
	mov [di], al
	inc di
	
	_1:
	mov flags, 00000b
	cmp fbuff, '`'
	jnz _2
	or flags, 01000b
	ret
	
	_2:
	cmp fbuff, "'"
	jnz _3
	or flags, 10000b
	ret
	
	_3:
	cmp fbuff, '['
	jnz _6
	
	mov BYTE PTR [di], OPCODE_JMP
	add di, 3 ; leaving 2 bytes for relative address
	
	add bp, 2
	mov [bp], di
	sub WORD PTR [bp], 2
	ret

	_6:
	cmp fbuff, ']'
	jnz _20
	
	mov BYTE PTR [di], OPCODE_RET
	inc di
	
	; Writing jump offset to start of the function
	mov ax, di
	mov si, [bp]
	sub ax, si
	sub ax, 2
	mov [si], ax
	
	; Popping jmp_labels stack
	sub bp, 2
	
	; Runtime: push function address to stack
	sub si, OFFSET end_of_program
	add si, 102h ; 100h + 2
	call write_push_value
	ret
	    
	_20: ; Checking for "simple commands" character
	mov si, 0
	_iter_table:
	mov bh, fbuff
	cmp simple_commands_table[si], bh
	jz _print_function
	add si, 3
	cmp si, 69 ; 23 commands * 3 bytes per command = nice
	jb _iter_table
	
	; Our character is either a variable or a whitespace
	xor ax, ax
	mov al, fbuff
	sub al, 'a'
	; We need this check to skip whitespace characters.
	; Characters above 'z' either are not allowed or have already been processed
	js _ret
	mov bx, 0002h
	mul bx
	
	mov BYTE PTR [di], OPCODE_MOV_AX
	inc di
	
	mov WORD PTR [di], ax
	add di, 2
	
	lea si, OPCODE_ADD_AX_BP
	movsw
	
	mov ax, OFFSET runtime_push
	call write_exec
	
	_ret:
	ret

	; Printing simple command
	_print_function:
	inc si
	mov ax, WORD PTR simple_commands_table[si]
	call write_exec
	ret
	
proc_symbol ENDP

write_exec PROC NEAR

	mov BYTE PTR [di], OPCODE_MOV_DI
	inc di
	
	mov WORD PTR [di], ax
	add di, 2
	
	lea si, OPCODE_CALL_DI
	movsw

	ret
	
write_exec ENDP

write_push_value PROC NEAR

	mov BYTE PTR [di], OPCODE_MOV_AX
	inc di
	
	mov WORD PTR [di], si
	add di, 2
	
	mov ax, OFFSET runtime_push
	call write_exec
	
	ret
	
write_push_value ENDP

read_command_line_argument PROC NEAR
	command_line_argument_read_loop:
	inc si
	cmp BYTE PTR [si], ah
	jnz command_line_argument_read_loop
	mov BYTE PTR [si], 0
	ret
read_command_line_argument ENDP
	
begin:
	
	; Setting di to the end of program; we will be writing our output there
	mov di, OFFSET end_of_program
	
	; We need a stack for jmp_labels
	; Allocating memory for it on regular stack
	; Maximum allowed function depth is 16
	; bp will point to the top of the stack
	sub sp, 32 ; = 2 * 16
	mov bp, sp
	dec bp
	
	; Initializing runtime
	mov cx, end_runtime - begin_runtime
	lea si, begin_runtime
	rep movsb
	
	mov cx, 3
	lea si, OPCODE_INIT_SE
	rep movsb
	
	; Reading source file name from first command line argument
	mov si, 81h
	mov ah, ' '
	call read_command_line_argument
	
	; Opening source file
	mov ax, 3D00h
	mov dx, 82h
	int 21h
	mov source_file_handle, ax
	
	; Reading executable file name from second (and last) command line argument
	mov dx, si
	inc dx
	mov ah, 0Dh
	call read_command_line_argument
	
	; Opening executable file
	mov ah, 3Ch
	mov cx, 0
	int 21h
	mov executable_file_handle, ax
	
	; Reading source file
	readfile:
	mov ah, 3Fh
	mov bx, source_file_handle
	lea dx, fbuff
	mov cx, 1
	int 21h
	cmp ax, 0
	jz eoff
	mov dl, fbuff
	cmp dl, 1Ah
	jz eoff
	call proc_symbol
	jmp readfile
	eoff:
	
	; Writing into executable file
	mov cx, 4
	lea si, OPCODE_END
	rep movsb
	
	mov bx, executable_file_handle
	mov ah, 40h
	mov dx, OFFSET end_of_program
	mov cx, di
	sub cx, dx
	int 21h
	
	; Closing both files
	mov ah, 3Eh
	int 21h
	mov bx, source_file_handle
	int 21h
	
	; Terminating the program
	mov ax, 4C00h
	int 21h

END
